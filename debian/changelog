python-escript (5.1-6) unstable; urgency=medium

  * Team upload.
  * [c8e84d99] Build fix for openmpi versions that aren't pure integers.
    Closes: #896104

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 19 Apr 2018 15:36:45 +0100

python-escript (5.1-5) unstable; urgency=medium

  * Team upload.
  * [8a6e107] Also ignore strict-overflow errors in s390x, albeit it happens
    only in ubuntu.
  * [9134bc2] Bump Standards-Version to 4.1.3, no changes needed.

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 01 Jan 2018 18:57:58 +0100

python-escript (5.1-4) unstable; urgency=medium

  * Team upload.
  * [8beca24] Try to fix FTBFS in powerpc and ppc64 by applying the same
    patch used for ppc64el (disabling a warning).
  * [0ea59fd] Build depend only on the default python and python3
    versions.  Closes: #867583
  * [096248d] Bump Standards-Version to 4.1.2, no changes needed.

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 11 Dec 2017 13:46:30 +0100

python-escript (5.1-3) unstable; urgency=medium

  * Team upload.
  * [48d47f2] Fix FTBFS on powerpc. Disable one warning

 -- Anton Gladky <gladk@debian.org>  Sun, 10 Dec 2017 12:03:41 +0100

python-escript (5.1-2) unstable; urgency=medium

  * Team upload.
  * [8a44bbe] Add missing space in the compiler flag. (Closes: #878496)
  * [39dc272] Remove unused patches

 -- Anton Gladky <gladk@debian.org>  Sat, 09 Dec 2017 23:35:51 +0100

python-escript (5.1-1) unstable; urgency=medium

  * Team upload.

  [ Joel Fenwick ]
  * [dae92b3] Upstream 5.1 - with modified /debian. (Closes: #878496)
  * [fed2b67] Spelling errors found by lintian
  * [ff2a920] Use libjs-mathjax instead of external site
  * [419c8e9] Suppress spurious warning on 32bit builds

  [ Anton Gladky ]
  * [e66da0f] Add section-field to each binary
  * [5c83249] Add python3-sphinx to BD
  * [b1bdc76] Remove myself from uploaders
  * [034da16] Apply cme fix dpkg

 -- Anton Gladky <gladk@debian.org>  Fri, 08 Dec 2017 20:48:12 +0100

python-escript (5.0-4) unstable; urgency=medium

  [ Joel Fenwick ]
  * [6f58a7e] Add sphinx-common to -doc dependencies. (Closes: #861023)

  [ Anton Gladky ]
  * [8713dfa] Replace latex-xcolor by texlive-latex-recommended. (Closes: #865245)
  * [7ebf074] Use compat level 10.
  * [70044aa] Set the highest level of hardening.

 -- Anton Gladky <gladk@debian.org>  Mon, 03 Jul 2017 22:02:53 +0200

python-escript (5.0-3) unstable; urgency=medium

  [ Paul Dreik ]
  * [b33279b] Fix FTBFS due to unused variables. (Closes: #860604)

 -- Anton Gladky <gladk@debian.org>  Wed, 19 Apr 2017 13:37:17 +0200

python-escript (5.0-2) unstable; urgency=medium

  [ Joel Fenwick ]
  * [a75d1b9] Fix moved openmpi lib problem (Closes: #848787)

  [ Anton Gladky ]
  * [b80952a] Apply cme fix dpkg.

 -- Anton Gladky <gladk@debian.org>  Thu, 05 Jan 2017 22:34:39 +0100

python-escript (5.0-1) unstable; urgency=medium

  * New version upload

 -- Joel Fenwick <j.oelpublic@gmail.com>  Mon, 19 Sep 2016 12:44:21 +1000

python-escript (4.2.0.1-4) unstable; urgency=medium

  [ Joel Fenwick ]
  * [96dc875] Protect against g++-6 (Closes: #831149)

 -- Anton Gladky <gladk@debian.org>  Thu, 11 Aug 2016 20:05:23 +0200

python-escript (4.2.0.1-3) unstable; urgency=medium

  [ Joel Fenwick ]
  * [462f3cc] Fix missing dep on python-numpy by python-escript-mpi
  * [295d646] Fix missing debug syms (and always build for sid options)
  * [73c3165] Allow extraction of tests from src tree
  * [ce27363] Fix py3clean issues, piuparts failure. (Closes: #826105, #826070)

  [ Anton Gladky ]
  * [2a88e77] Apply cme fix dpkg.
  * [e0c742a] Add VCS-fields.

 -- Anton Gladky <gladk@debian.org>  Tue, 21 Jun 2016 23:02:46 +0200

python-escript (4.2.0.1-2) unstable; urgency=medium

  [ Joel Fenwick ]
  * [35b8cd4] Remove dud file + shorten copyright file

  [ Anton Gladky ]
  * [9f90d8c] Add libnetcdf-dev to build-depends. (Closes: #825874)
  * [e75b88b] Add myself to uploaders.

 -- Anton Gladky <gladk@debian.org>  Wed, 01 Jun 2016 21:21:46 +0200

python-escript (4.2.0.1-1) unstable; urgency=low
  * First debian release of escript. (Closes: 783157)
 -- Joel Fenwick <j.oelpublic@gmail.com>  Mon, 04 Apr 2016 10:49:00 +1000

Old Changelog:
python-escript (4.1-1) experimental; urgency=low
  * First debian release of escript. Upstream version 4.1.
  * closes: 783157
 -- Joel Fenwick <j.oelpublic@gmail.com>  Thu, 23 Apr 2015 17:28:21 +1000

python-escript (4.0) wheezy; urgency=low
  * Release 4.0
  * User guide has summary of changes
 -- Joel Fenwick <joelfenwick@uq.edu.au>  Thu, 16 Dec 2014 09:34:00 +1000

python-escript (3.4.2) wheezy; urgency=low
  * Release 3.4.2
  * User guide has summary of changes
 -- Joel Fenwick <joelfenwick@uq.edu.au>  Thu, 1 May 2014 12:00:00 +1000

python-escript (3.4.1) wheezy; urgency=low
  * Release 3.4.1
  * User guide has summary of changes
 -- Joel Fenwick <joelfenwick@uq.edu.au>  Thu, 12 December 2013 13:15:00 +1000

python-escript (3.4) wheezy; urgency=low
  * Release 3.4
 -- Joel Fenwick <joelfenwick@uq.edu.au>  Wed, 5 June 2013 08:47:00 +1000


escript (3.3.1-1) wheezy; urgency=low
  * Release 3.3.1
  * User guide has summary of changes
 -- Joel Fenwick <joelfenwick@uq.edu.au>  Thu, 1 November 2012 16:00:00 +1000

escript (3.3-1) squeeze; urgency=low
  * Release 3.3
  * User guide has more details.
 -- Joel Fenwick <joelfenwick@uq.edu.au>  Wed, 31 October 2012 15:14:00 +1000

escript (3.1-1) lenny; urgency=low
  * Release 3.1.
  * Lazy being let out of its cage. Various other tweaks and features. See user guide for details.
 -- Joel Fenwick <joelfenwick@uq.edu.au>  Wed, 31 October 2012 15:13:00 +1000


escript (3.0-1) lenny; urgency=low
  * Release 3.0.
  * No pyvisi anymore.
 -- Joel Fenwick <joelfenwick@uq.edu.au>  Wed, 31 October 2012 15:12:00 +1000

escript(~3.0-1) lenny; urgency=low
  * Post release 2.0.
  * By JFenwick (revision 2456)
  * Switched from numarray to numpy. A number of post release bugs in the documentation
  * and examples files fixed.
 -- Joel Fenwick <joelfenwick@uq.edu.au>  Wed, 31 October 2012 15:11:00 +1000

escript (~2.0-1) lenny; urgency=low
  * Initial packaging attempt.
  * By JFenwick (revision 2288:2290M)
 -- Joel Fenwick <joelfenwick@uq.edu.au>  Wed, 31 October 2012 15:10:00 +1000
